# Makefile
OPTS=-Wall -Wextra -Wconversion -pedantic -std=c99
NB_BITS=$(shell getconf LONG_BIT)
SRC=src/c/main.c src/c/functions.c
OBJS=main.o functions.o
DEBUG_OPTS=
EXEC=mallet_guilbault_findgrep
%.o=$(OBJS)

# Select the good lib (32 or 64 bits)
ifeq ($(NB_BITS),64)
    LIB=src/lib/liblists64.a
else
    LIB=src/lib/liblists32.a
endif

# Meta targets
debug: DEBUG_OPTS=-g
debug: EXEC=debug

first: mallet_guilbault_findgrep

all: mallet_guilbault_findgrep rapport.pdf

clean: clean-objs
	@rm -f ./mallet_guilbault_findgrep
	@rm -f ./debug
	@echo "All cleaned."

clean-objs:
	@rm -f ./*.o

# Main Targets

mallet_guilbault_findgrep debug: $(OBJS) $(LIB)
	gcc -o $(EXEC) $(OBJS) $(LIB) -D_BSD_SOURCE -Isrc/h $(DEBUG_OPTS) -lpthread $(OPTS)
	
rapport.pdf: rapport/rapport.tex rapport/projetC.bib
	cd rapport;\
	pdflatex -shell-escape -interaction nonstopmode rapport.tex;\
	biber rapport;\
	pdflatex -shell-escape -interaction nonstopmode rapport.tex

# Sub-targets
main.o functions.o:
	gcc -c -o $@ $< -D_BSD_SOURCE -Isrc/h $(DEBUG_OPTS) -lpthread $(OPTS)

# Dependencies
main.o: src/c/main.c src/h/functions.h src/h/lists.h
functions.o: src/c/functions.c src/h/functions.h src/h/lists.h
