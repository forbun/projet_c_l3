#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#define MAX_PATH_SIZE 1024

#include <stdio.h>
#include <pthread.h>

typedef struct thread_array thread_array;

/** explore_struct contains all informations which needs a thread.
 *	The creation of a thread use this, as an argument of explore().
 *	It contains :
 *		- The path of the working directory of the thread
 * 		- The path of the named pipe
 *		- All mutex (pipe / output / thread_array access)
 *		- The name of researched file
 *		- The name of researched string in a .txt file
 *		- The thread ID of this thread (it is used when main() join a thread,
 *			for comparison)
 *		- The thread_array structure used in main() (when a thread add its
 *			thread ID in the toDel-list)
 */
typedef struct explore_struct{
	char* dirpath;
	pthread_mutex_t* printlock;
	char* backpipe;
	pthread_mutex_t* pipelock;
	char* target_file;
	char* target_text;
	thread_array* th_array;
	pthread_mutex_t* th_mut;
	pthread_t* threadId;
}explore_struct;

// See documentation of each function in functions.c
int look_for_string(FILE*,char*);

void printattr(char*);

void* explore(void* myparams);

unsigned int nb_running_threads(thread_array*,pthread_mutex_t*);

thread_array* init_thread_array(unsigned int nbthreads);

void free_thread_array(thread_array *toFree);

int add_thread(thread_array* myarray, explore_struct* params);

explore_struct* get_thread(thread_array* myarray,unsigned int idx);

int del_thread(thread_array* marray, pthread_t* tToDel);

unsigned int get_max_thread(thread_array* marray);

explore_struct*
init_explore_struct(char*,pthread_mutex_t*,char*,pthread_mutex_t*,char*,char*,thread_array*,pthread_mutex_t*,pthread_t*);

char* get_abs_path (char* filename, char* dirpath);

#endif
