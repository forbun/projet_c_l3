#ifndef LIST_H
#define LIST_H
#include<stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

typedef struct treenode treenode;
typedef struct linkedlist linkedlist;
typedef struct item item;

///this is the basic container for lists, stack and queues

/**to work correctly it needs a pointer to the structure you want to add to your list
to unlock further features you should also have a way to tell if any item is "bigger" or "smaller"
than any other*/
typedef struct hashtable hashtable;

/// this structure is a wrapper to manipulate lists
typedef struct stack stack;
typedef struct queue queue;
typedef struct hash hash;
//Creation functions
stack* createstack(void*,size_t);
linkedlist* createlist(void *,void*,void*,size_t);
queue* createQueue(void*,void*,size_t);
linkedlist* listCpy(linkedlist*);
stack* stackCpy(stack*);
queue* queueCpy(queue*);
linkedlist* listSplit(linkedlist* old,item*);
stack* stackSplit(stack*,item*);

//Mutation functions
item* additem(linkedlist*,void*);
item* getitem(linkedlist* liste,int number);
item* gettip(stack* monstack);
void swap(item*,item*);
item* insert(item*,int,void*,linkedlist*);
item* createItem(void*,size_t);
void* delitemByNum(linkedlist*,int);
int delitemByData(linkedlist*,void*,int);
item* push(stack*, void*);
item* pop(stack*);
void listFusion(linkedlist**,linkedlist**);
void stackFusion(stack*,stack*);
item* enqueue(queue*,void*);
item* dequeue(queue*);

//accessor
item* getnext(item*);
void* getdata(item*);
int getsize(linkedlist*);

//test functions
int Lempty(linkedlist*);
int Sempty(stack*);
int Qempty(queue*);

//deletion functions
void delstack(stack**);
void delQueue(queue**);
void dellist(linkedlist**);

//selection functions
item* getfirst(linkedlist*);
item* getlast(linkedlist*);
void prioritize(queue*);

//storage functions
void save_Q(queue*,char*);
void save_S(stack*,char*);
void save_L(linkedlist*,char*);
queue* load_Q(char*);
stack* load_S(char*);
linkedlist* load_L(char*);

//private
void heapify(linkedlist*);
void renumber(item*,int);//renumber from item* counting from int
void queueFusion(queue*,queue*);//if the first one is prioritized then the fusion shall be
queue* queueSplit(queue*,item*);
void introsort(linkedlist*);
void sort(linkedlist*,item*,item*,int,int);
item* quickselect(linkedlist*,void*);
//private
item* recselect(linkedlist*, item*,item*,void*);
/*
//PRIVATE
hashtable* hashList(linkedlist*,int);
*/
#endif
