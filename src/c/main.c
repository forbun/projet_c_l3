#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

#include "functions.h"
#include "lists.h"

/** Create a thread, with all needed content.
 */
void createThread(char* dirpath, pthread_mutex_t* printMutex, char* backpipe,
        pthread_mutex_t* pipeMutex, char* targetFile, char* targetText,
        thread_array* thArray, pthread_mutex_t* thMutex) {

        pthread_t* tId = (pthread_t*)malloc(sizeof(pthread_t));
        explore_struct* params = init_explore_struct(dirpath, printMutex,
            backpipe, pipeMutex,targetFile, targetText, thArray, thMutex, tId);
        pthread_create(tId, NULL, explore, params);
        add_thread(thArray, params);
}
//==============================================================
int main(int argc,char** argv){
    char* thread_num = NULL;
    char* topdir = NULL;
    char* filename = NULL;
    char* match_text = NULL;

    // Setup params passed in shell
    int c;
    while((c = getopt(argc,argv,"f:d:t:m:h"))!=-1){
        switch(c){
            case 'f':
                filename = optarg;
                break;

            case 'd':
                topdir = optarg;
                break;

            case 't':
                thread_num = optarg;
                break;

            case 'm':
                match_text = optarg;
                break;

            case 'h':
                printf("use me -f filename -d topdir -t max_thread_number\
                and -m target_text\n");
                exit(0);
				break;

			default:
				printf("use -h to get some help\n");
				abort();
				break;
        }
    }


    char *dir_abs_path = malloc(MAX_PATH_SIZE*sizeof(char));
    realpath(topdir,dir_abs_path);

    //if thread number undefined set it to 8
    if(thread_num == NULL){
        thread_num = "30";
    }

    //if backpipe file does not exist, create it
    if( !(access( "backpipe", F_OK ) != -1) ) {
        mkfifo("backpipe",0700);
    }

    char pipepath[MAX_PATH_SIZE];
    realpath("backpipe",pipepath);

    // open the named pipe
    // TODO : "backpipe" == pipepath ???
    int mypipe = open("backpipe",O_RDONLY|O_NONBLOCK);
	if(mypipe == -1){
		fprintf(stderr,"Could not open backpipe,\
		aborting\n");
		abort();
	}

    // Initialize mutex
    pthread_mutex_t pipe_mutex,print_mutex,thread_array_mutex;
    pthread_mutex_init(&pipe_mutex,NULL);
    pthread_mutex_init(&print_mutex,NULL);
	pthread_mutex_init(&thread_array_mutex,NULL);

	thread_array* my_thread_array = init_thread_array((unsigned int) atoi(thread_num));

    // Create the 1st thread : the top directory
	createThread(dir_abs_path,&print_mutex,pipepath,&pipe_mutex,filename,match_text,my_thread_array,&thread_array_mutex);

	// There is a "break" below
	while(1){
        // Receive the size of the string (try it)
		size_t total = 0;

        pthread_mutex_lock(&pipe_mutex);
        ssize_t bytesRead = read(mypipe,&total,sizeof(total));

        // We enter in this condition only when the piece of data are sent, because of the mutex
		if(bytesRead == sizeof(total)){
            // We have received the 1st part : now we read the second part : the path
            char *line = malloc(MAX_PATH_SIZE*sizeof(char));
			line[0] = '\0'; // Assume this is a 0-length string before

            ssize_t bytesRead = read(mypipe,line,total);
            pthread_mutex_unlock(&pipe_mutex);

            if(bytesRead == -1) {
                pthread_mutex_lock(&print_mutex);
                fprintf(stderr,"l%d - error while reading mypipe",__LINE__);
                fprintf(stderr,"%s\n",strerror(errno));
                pthread_mutex_unlock(&print_mutex);

                exit(1);
            }
            // Assure the end of the path with a NULL char
			line[total] = '\0';
			assert(strcmp(line,"") != 0);

            // Creation of the thread, based of the content of line
			createThread(line,&print_mutex,pipepath,&pipe_mutex,filename,match_text,my_thread_array,&thread_array_mutex);
		} else {
            // Free properly pipe mutex, because token it before if(...)
            pthread_mutex_unlock(&pipe_mutex);

            // Check if there is always a launched thread
            // If not ==> END
            if(nb_running_threads(my_thread_array,&thread_array_mutex) == 0) {
                break;
            }
        }

        // This loop permit to wait if there is enough launched threads
		while(nb_running_threads(my_thread_array,&thread_array_mutex) ==
		get_max_thread(my_thread_array)){

		}
	}

    // Free all ressources
    pthread_mutex_destroy(&print_mutex);
    pthread_mutex_destroy(&pipe_mutex);

    free_thread_array(my_thread_array);
	close(mypipe);
	remove("backpipe");

    remove(pipepath);//cleanup

    return 0;
}
