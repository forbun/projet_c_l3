#include "functions.h"

#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include "lists.h"

/** thread_array store all information about threads :
 *		- parameters of threads (contains the thread ID)
 *		- number of threads runnning
 *		- max of this
 *		- a list of finished thread, but not yet joined
 */
struct thread_array{
	explore_struct** array;
	unsigned int nbthread;
	unsigned int maxthread;
	linkedlist* todel;
};
//===================================================================
/** Return the list of all "files" in a directory.
 */
static linkedlist* get_files(DIR* mydir){
	struct dirent* dp;
	linkedlist* myfiles = createlist(NULL,NULL,NULL,sizeof(struct dirent));
	while( (dp = readdir(mydir)) ){
		item* ret = additem(myfiles,dp);
		if(ret == 0){
			fprintf(stderr,"Error, could not add file to linkedlist");
			exit(1);
		}
	}
	return myfiles;
}
//==============================================================
/** Check if a "file" is a directory
 */
static int is_dir(struct dirent* dp){
	return (dp->d_type==DT_DIR && strcmp(dp->d_name,".") != 0 &&
		strcmp(dp->d_name,"..") != 0);
}
//==============================================================
/** Check is a "file" end with ".txt".
 */
static int is_txt(struct dirent* dp){
	char* tmp = ".txt";
	return (tmp[3] == dp->d_name[strlen(dp->d_name)-1] &&\
					tmp[2] == dp->d_name[strlen(dp->d_name)-2] &&\
					tmp[1] == dp->d_name[strlen(dp->d_name)-3] &&\
					tmp[0]==dp->d_name[strlen(dp->d_name)-4]);
}
//==============================================================
/** Write in the pipe the path of a directory.
 */
static void write_request(pthread_mutex_t* pipelock, char* pipepath,char*
name,char* path){
	char* totalpath = get_abs_path(name,path);
	int fd = open(pipepath,O_WRONLY);
	pthread_mutex_lock(pipelock);
	size_t length = strlen(totalpath);
	write(fd,&length,sizeof(length));
	write(fd,totalpath,length);
	close(fd);
	pthread_mutex_unlock(pipelock);
	free(totalpath);
}
//==============================================================
/** Print a filepath, with its attributes.
 */
static void print_statement(char* name, char* path, pthread_mutex_t*
printlock,char* statement){
	char* totalpath = get_abs_path(name,path);
	pthread_mutex_lock(printlock);
	printattr(totalpath);
	fprintf(stdout," %s\n",totalpath);

	if(statement != NULL){
		fprintf(stdout,"%s",statement);
	}
	pthread_mutex_unlock(printlock);
	free(totalpath);
}
//==============================================================
/** Look in an open file if a string is in or not.
 */
int look_for_string(FILE* fd,char* string){
	char read[512];
	int readback = 0;
	int notfound = 1;
	int line_cpt = 0;
	int at_end = 0;
	while(notfound == 1 && at_end == 0){
		// Read the 512-bytes block & check return
		if(fread(read,512,1,fd)!=1){
			at_end = 1;
		}

		// Check cut word at the end of the block;
		// Move the file cursor at the beginning of this word.
		if(strlen(read) == 512){
			if(read[511] != ' ' && read[511] != '\n' && read[511] != EOF){
				int i = 0;
				while(read[511-i] != ' ' && read[511-i] != '\n' && read[511-i] != EOF){
					i++;
				}
				readback = i;
			}
		}
		fseek(fd,-readback,SEEK_CUR);

		// Look if we find the searched string, according to a light version
		// of the Boyer-Moore substring search algorithm
		size_t i = 0;
		size_t j = strlen(string)-1;
		for(i = j; i < strlen(read);i++){
			// Count the line, for the output
			if(read[i] == '\n'){
				line_cpt++;
			}
			// Comparison
			if(read[i] == string[j]){
				size_t k = 0;
				int inner_not_found = 1;
				// Begins with the end of the word,
				// then look back.
				for(k = 0; k < strlen(string);k++){
					if(read[i-k] != string[j-k]){
						break;
					}
					else if(k == strlen(string)-1){
						inner_not_found = 0;
					}
				}
				notfound = inner_not_found;
				// Break if we found an occurence of string
				if(notfound == 0){
					break;
				}
			}
		}
		if(readback > 0){
			line_cpt--;
		}
	}

	// Return the line where we found the string, or -1 otherwise.
	if(notfound){
		return -1;
	}else{
		return line_cpt;
	}
}
//=============================================================
/** Print attributes of a file, i.e. permisions here.
 */
void printattr(char* filename){
	struct stat buf;
	stat(filename,&buf);

	// NOTA : it is easier and more concise to do a ternary operator
	// than an if ... else ... block
	fprintf(stdout, (S_ISDIR(buf.st_mode)) ? "d" : "-");
	fprintf(stdout, (buf.st_mode & S_IRUSR) ? "r" : "-");
	fprintf(stdout, (buf.st_mode & S_IWUSR) ? "w" : "-");
	fprintf(stdout, (buf.st_mode & S_IXUSR) ? "x" : "-");
	fprintf(stdout, (buf.st_mode & S_IRGRP) ? "r" : "-");
	fprintf(stdout, (buf.st_mode & S_IWGRP) ? "w" :"-");
	fprintf(stdout, (buf.st_mode & S_IXGRP) ? "x" :"-");
	fprintf(stdout, (buf.st_mode & S_IROTH) ?"r" : "-");
	fprintf(stdout, (buf.st_mode & S_IWOTH)? "w" : "-");
	fprintf(stdout, (buf.st_mode &S_IXOTH) ? "x" : "-");
}
//=============================================================
/** The function called with threads : explore a directory,
 *	parse files, and do what it needs to do.
 */
void* explore(void* myparams){
	// Cast myparams, then simplify access of fields of myparams
	explore_struct* params = (explore_struct*)myparams;

	char* dirpath = params->dirpath;
	pthread_mutex_t* printlock = params->printlock;
	char* backpipe = params->backpipe;
	pthread_mutex_t* pipelock = params->pipelock;
	char* target_file = params->target_file;
	char* target_text = params->target_text;
	thread_array* myarr = params->th_array;
	pthread_mutex_t* thmut = params->th_mut;
	pthread_t* tId = params->threadId;
	struct dirent* dp;

	// Look all "files" in dirpath
	DIR* mydir = opendir(dirpath);
	if(mydir == NULL){

		pthread_mutex_lock(printlock);
		fprintf(stderr,"error while opening =%s= of length %lu\n",dirpath,strlen(dirpath));
		fprintf(stderr,"%s\n",strerror(errno));
		pthread_mutex_unlock(printlock);

		exit(1);
	}
	linkedlist* myfiles = get_files(mydir);

	// Parse each file in myfiles list
	item* cur = getfirst(myfiles);
	while(cur != 0){
		int found = 0;
		dp = ((struct dirent*)(getdata(cur)));
		// is the file a directory ?
		if(is_dir(dp)){
			if(is_txt(dp)){
				if(target_file != NULL && strcmp(dp->d_name,target_file)==0){
					char* totalpath = get_abs_path(dp->d_name,params->dirpath);

					pthread_mutex_lock(printlock);
					fprintf(stdout,"%s <REP>",totalpath);
					printattr(totalpath);
					pthread_mutex_unlock(printlock);

					free(totalpath);
				}
			}
			write_request(pipelock,backpipe,dp->d_name,params->dirpath);
		}
		// If we search a file, do the name match ?
		else if(target_file != NULL && dp->d_type != DT_DIR){
			if(strcmp(dp->d_name,target_file)==0){
				if(target_text != NULL){
					char* totalpath = get_abs_path(dp->d_name,params->dirpath);
					FILE* fd = fopen(totalpath,"r");
					found = look_for_string(fd,target_text);
					fclose(fd);
					free(totalpath);
				}
				if(found != -1){

					print_statement(dp->d_name,params->dirpath,printlock,NULL);
				}
			}
		}
		// If we search a text in a .txt file, can we find the target within ?
		else if(target_text != NULL && dp->d_type != DT_DIR){
			if(target_text != NULL && strlen(dp->d_name) >= strlen(".txt")){
				if(is_txt(dp)){

					// Search the target_text in this file
					char* totalpath = get_abs_path(dp->d_name,params->dirpath);
					FILE* fd = fopen(totalpath,"r");
					found = look_for_string(fd,target_text);
					fclose(fd);
					if(found != -1){

						pthread_mutex_lock(printlock);
						char* totalpath = get_abs_path(dp->d_name,params->dirpath);
						fprintf(stdout,"found %s at line %d of file %s : ",
						target_text,found,dp->d_name);
						printattr(totalpath);
						fprintf(stdout,"\n");
						free(totalpath);
						pthread_mutex_unlock(printlock);

					}
				}
			}
		}
		// Prepare the next loop : next file.
		cur = getnext(cur);
	}

	// Free ressources
	closedir(mydir);

	while(!Lempty(myfiles)) {
		explore_struct *data = delitemByNum(myfiles, 0);
		free(data);
	}
	dellist(&myfiles);

	// Add the thread ID (stored in tId) in the toDel-list
	pthread_mutex_lock(thmut);
	additem(myarr->todel,tId);
	pthread_mutex_unlock(thmut);

	return NULL;
}
//=============================================================
/** Get the number of running threads and, if it exists, join threads
 *	witch a finished (or close of it).
 */
unsigned int nb_running_threads(thread_array* my_threads,pthread_mutex_t* lock){

	pthread_mutex_lock(lock);
	unsigned int result = my_threads->nbthread;
	// Check if some threads are finished
	while(!Lempty(my_threads->todel)){
		pthread_t* tTodel = delitemByNum(my_threads->todel, 0);
		pthread_mutex_unlock(lock);
		// try to join *tTodel
		int retCode = pthread_join(*tTodel, NULL);
		if(retCode) {
			// Can't join : break
			free(tTodel);
			break;
		} else {
			pthread_mutex_lock(lock);
			del_thread(my_threads,tTodel);
		}
		free(tTodel);

	}
	pthread_mutex_unlock(lock);

	return result;
}

//=============================================================
/** The thread_array constructor.
 */
thread_array* init_thread_array(unsigned int nbthreads){
	thread_array* my_threads = (thread_array*)malloc(sizeof(thread_array));
	my_threads->nbthread = 0;
	my_threads->maxthread = nbthreads;
	my_threads->array =
	(explore_struct**)malloc(sizeof(explore_struct*)*my_threads->maxthread);
	my_threads->todel = createlist(NULL,NULL,NULL,sizeof(pthread_t));
	return my_threads;
}

//=============================================================
/** The thread_array destructor.
 */
void free_thread_array(thread_array *toFree) {
	free(toFree->array);
	dellist(&(toFree->todel));
	free(toFree);
}
//=============================================================
/** Add a thread in a thread_array structure.
 */
int add_thread(thread_array* myarray, explore_struct* params){

	int res = 1;
	if(myarray->nbthread < myarray->maxthread){
		myarray->array[myarray->nbthread] = params;
		myarray->nbthread++;
		res = 0;
	}

	return res;
}

//=============================================================
/** Return the idx-th thread in the thread_array structure.
 *	@deprecated This is not used in this program.
 */
explore_struct* get_thread(thread_array* myarray,unsigned int idx){
	explore_struct *res = NULL;
	if(idx <= myarray->nbthread){
		res = myarray->array[idx];
	}
	return res;
}
//=============================================================
/** Free ressources of the thread of ID tToDel,
 *	then rearrange the array of thread.
 */
int del_thread(thread_array* marray, pthread_t* tToDel){
	int res = 1;
	if(marray->nbthread > 0){
		unsigned int i,j;

		// Search which pthread_t needs to be destroyed
		i = 0;
		while(i < marray->nbthread) {
			// Check if this is the good thread
			if(*marray->array[i]->threadId == *tToDel) {
				free(marray->array[i]->threadId);
				free(marray->array[i]->dirpath);
				free(marray->array[i]);
				marray->nbthread--;

				// Left shift from i to end
				for(j = i; j <marray->nbthread;j++){
					marray->array[j] = marray->array[j+1];
				}

				i--;
				res = 0;
			}
			i++;
		}
	}

	return res;
}
//==============================================================
/** Get the number of simultaneous threads authorized.
 */
unsigned int get_max_thread(thread_array* marray){
	return marray->maxthread;
}

//==============================================================
/** The explore_struct constructor.
 */
explore_struct* init_explore_struct(char* dirpath,pthread_mutex_t*
printlock,char* backpipe,pthread_mutex_t* pipelock,char* target_file,char*
target_text,thread_array* marr, pthread_mutex_t* th_mut,pthread_t* tId){
	explore_struct* res = (explore_struct*)malloc(sizeof(explore_struct));
	res->dirpath = dirpath;
	res->printlock = printlock;
	res->backpipe = backpipe;
	res->pipelock = pipelock;
	res->target_file = target_file;
	res->target_text = target_text;
	res->th_mut = th_mut;
	res->th_array = marr;
	res->threadId = tId;
	return res;
}
//==============================================================
/** Concatenate a filename with its dirpath, speared with a '/'.
 */
char* get_abs_path (char* filename, char* dirpath){
	char* totalpath = (char*) malloc(sizeof(char)*MAX_PATH_SIZE);
	totalpath[0] = '\0';
	strcat(totalpath,dirpath);
	strcat(totalpath,"/");
	strcat(totalpath,filename);
	return totalpath;
}
